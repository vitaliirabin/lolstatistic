package com.example.lolstatistic.match

class DataMatch {
    val dataVersion:Int?=null
    val matchId:String?=null
    val participants:List<String>?=null
}