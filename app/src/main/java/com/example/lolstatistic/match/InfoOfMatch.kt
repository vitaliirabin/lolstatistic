package com.example.lolstatistic.match

class InfoOfMatch {
    val gameCreation: Long? = null
    val gameDuration: Int? = null
    val gameId: Int? = null
    val gameMode: String? = null
    val gameName: String? = null
    val gameStartTimestamp: Long? = null
    val gameType: String? = null
    val gameVersion: Long? = null
    val mapId: Int? = null
    val participants: List<Participant>? = null
}
