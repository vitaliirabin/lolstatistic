package com.example.lolstatistic.statistic

class StatisticModel {
    val platformId:String?=null
    val gameId: Long?=null
    val champion: Int?=null
    val queue: Int?=null
    val season: Int?=null
    val timestamp: Long?=null
    val role: String?=null
    val lane: String?=null
}